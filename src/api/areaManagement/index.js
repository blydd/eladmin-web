import request from '@/utils/request'

// 小区列表
export function getArealist() {
  return request({
    url: 'api/area',
    method: 'get'
  })
}

// 楼栋列表
export function getBuildList(areaDetail) {
  return request({
    url: 'api/area/' + areaDetail.id + '/build/',
    method: 'get'
  })
}

// 房间列表
export function getRoomList(buildDetail) {
  return request({
    url: 'api/area/' + buildDetail.areaId + '/build/' + buildDetail.id + '/room?current=1&size=10',
    method: 'get'
  })
}

// 新增小区
export function saveArea(data) {
  return request({
    url: 'api/area',
    method: 'post',
    data: data
  })
}
//   删除小区
export function deleteArea(id) {
  return request({
    url: 'api/area/' + id,
    method: 'delete'
  })
}

//   删除房间
export function deleteRoom(roomDetail) {
  return request({
    url: 'api/area/1/build/' + roomDetail.buildId + '/room/' + roomDetail.id,
    method: 'delete'
  })
}

//   删除楼栋
export function deleteBuild(buildDetail) {
  return request({
    url: 'api/area/' + buildDetail.areaId + '/build/' + buildDetail.id,
    method: 'delete'
  })
}

// 新增楼栋
export function addBuild(data) {
  return request({
    url: 'api/area/' + data.areaId + '/build',
    method: 'post',
    data
  })
}
// 新增房间
export function addRoom(data) {
  return request({
    url: 'api/area/' + data.areaId + '/build/' + data.buildId + '/room',
    method: 'post',
    data
  })
}
